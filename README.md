# DevOps Test Task 
### Roman Solovev r.solovev@innopolis.ru

### Links:
* Deployed app - https://www.testtaskopsdev.ml
* Gitlab repository - https://gitlab.com/rsolovev/testtaskopsdev.ml
* Source repository  - https://github.com/nsky80/ttps

### Things done:
- Project chosen:
    - ML model in Django
- Deployed using docker-compose
    - `nginx:latest` image as httpd and to serve static files
    - `ubuntu:16.04` image for Django - as project involves xgboost and other ML libraries (pip needs to build them), I was not able to use `python:*` images
- Available at https://www.testtaskopsdev.ml
    - AWS used as VPS
    - DNS provided by CloudFlare
    - SSL/TLS also provided by CloudFlare

- Gitlab CI integrated
    - only one stage: deploy on production
    - this stage is manual
    - uses `alpine` gitlab-runner to connect to server via ssh to rebuild and restart containers with recent changes

### Some project info:
- Django based Machine Learning Project which predicts expected tourists in Future Quarters
- Not mine, taken and modified to achieve task purposes  
#### Example of use   
![](https://i.imgur.com/mOWFwwF.png)
![](https://i.imgur.com/tGkrvS7.png)
