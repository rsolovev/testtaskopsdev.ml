# As project uses xgboost, python:* images are not suitable, thus using ubuntu image
FROM ubuntu:16.04

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev
 
RUN mkdir /app
WORKDIR /app
COPY app/requirements.txt /app/
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
COPY ./app /app/
